$(document).ready(function(){
	// **** code for feature section slider******
	$('.feature-list').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 3,
		dots:true
	});

	// ***** code for work section filter*****
	$('#all').addClass('design');
	$('.all').addClass('active');
	$('#all').click(function(){
		console.log('clicked');
		$('.all').addClass('active');
		$('.filter-tabs li').removeClass('design');
		$(this).addClass('design');
	});
	$('#branding').click(function(){
		$('.all').removeClass('active');
		$('.branding').addClass('active');
		$('.filter-tabs li').removeClass('design');
		$(this).addClass('design');
	});
	$('#web').click(function(){
		$('.all').removeClass('active');
		$('.web').addClass('active');
		$('.filter-tabs li').removeClass('design');
		$(this).addClass('design');
	});
	$('#logodesign').click(function(){
		$('.all').removeClass('active');
		$('.logodesign').addClass('active');
		$('.filter-tabs li').removeClass('design');
		$(this).addClass('design');
	});
	$('#photography').click(function(){
		console.log('clicked');
		$('.all').removeClass('active');
		$('.photography').addClass('active');
		$('.filter-tabs li').removeClass('design');
		$(this).addClass('design');
	});

	// *********code for team slider**********

	$('.team-list').slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 4,
		dots:true
	});

});























